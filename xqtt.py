#!/usr/bin/python
from __future__ import division
import serial, datetime, time, sys, threading, logs, utils, math, os, json, ConfigParser, ast, pytz, socket
import paho.mqtt.client as mqtt
from xbee import XBee, ZigBee
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from SocketServer import ThreadingMixIn
from json2html import *

### Get run mode

if len(sys.argv) > 1:
	runmode = sys.argv[1]
elif len(sys.argv) == 1:
	runmode = 'normal'

logger = logs.setup_logger('xqtt')
logger.warning("Program Started...")
starttime = time.time()

def get_threads():
	threads = []
	main_thread = threading.current_thread()
	for t in threading.enumerate():
		if t is main_thread:
			continue
			print t.getName()
		threads.append(t.getName())
	return threads

def check_network():
	loop = 1
        while loop == 1:
		try:
 			s = socket.create_connection((socket.gethostbyname(config['mqtt_hostname']), config['mqtt_port']), 2)
			logger.warning("Network checks passed!, '%s' is reachable." % config['mqtt_hostname'])
			loop = 0
		except Exception:
			logger.warning("Network checks failed!, stalling until MQTT Broker '%s' becomes reachable..." % config['mqtt_hostname'])
			time.sleep(15)

### Load radio_db file and settings 
def radio_db_load(mode = 'file'):
	global radio_db
	global ed_status
	global radio_db_dict
	radio_db_dict = {}
	ed_status = {}
	# Load radio_db config file
	radio_db = ConfigParser.RawConfigParser()
	radio_db.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'radio_db.cfg'))
	# Fill radio_db dictionary
	for section in radio_db.sections():
		radio_db_dict[section] = {}
		for key, value in radio_db.items(section):
			radio_db_dict[section][key] = value
	# Fill end device status dictionary
	for device, attributes in radio_db_dict.iteritems():
		parameters = {}
		for parameter, value in attributes.iteritems():
			parameters.update({'mac':device})
			if parameter == 'id':
				deviceid = value
				ed_status.update({value:{}})
			else:
				parameters.update({parameter:value})
		ed_status.update({deviceid:{'Settings':parameters, 'Diagnostics':{}, 'Networking':{}, 'I/O Settings':{}, 'Security':{}, 'Addressing':{}, 'Rf Interfacing':{}, 'Seep Modes':{}}})

def environment_load():
	global config
	global stats
	global transceivers
	global mqtt_gateways
	global at_commands
	global rr_database
	global queue
	global priority_queue
	config = {}
	rr_database = {}
	queue = {}
	priority_queue = utils.queue()
	config_file = ConfigParser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'hostid':utils.chop_comment(config_file.get('host', 'id')),
	'mqtt_enabled':config_file.getboolean('mqtt', 'enabled'),
	'mqtt_hostname':utils.chop_comment(config_file.get('mqtt', 'hostname')),
	'mqtt_port':int(utils.chop_comment(config_file.get('mqtt', 'port'))),
	'mqtt_username':utils.chop_comment(config_file.get('mqtt', 'username')),
	'mqtt_pass':utils.chop_comment(config_file.get('mqtt', 'password')),
	'mqtt_publish_topic':utils.chop_comment(config_file.get('mqtt', 'publish_topic')),
	'mqtt_subscribe_topic':utils.chop_comment(config_file.get('mqtt', 'subscribe_topic')),
	'mqtt_publish_format':utils.chop_comment(config_file.get('mqtt', 'format')),
	'influxdb_enabled':config_file.getboolean('influxdb', 'enabled'),
	'influxdb_hostname':utils.chop_comment(config_file.get('influxdb', 'hostname')),
	'influxdb_port':int(utils.chop_comment(config_file.get('influxdb', 'port'))),
	'influxdb_username':utils.chop_comment(config_file.get('influxdb', 'username')),
	'influxdb_password':utils.chop_comment(config_file.get('influxdb', 'password')),
	'influxdb_dbname':utils.chop_comment(config_file.get('influxdb', 'database')),
	'elasticsearch_enabled':config_file.getboolean('elasticsearch', 'enabled'),
	'elasticsearch_hostname':utils.chop_comment(config_file.get('elasticsearch', 'hostname')),
	'elasticsearch_port':int(utils.chop_comment(config_file.get('elasticsearch', 'port'))),
	'elasticsearch_username':utils.chop_comment(config_file.get('elasticsearch', 'username')),
	'elasticsearch_password':utils.chop_comment(config_file.get('elasticsearch', 'password')),
	'elasticsearch_index':utils.chop_comment(config_file.get('elasticsearch', 'index')),
	'kapacitor_enabled':config_file.getboolean('kapacitor', 'enabled'),
	'kapacitor_hostname':utils.chop_comment(config_file.get('kapacitor', 'hostname')),
	'kapacitor_port':utils.chop_comment(config_file.get('kapacitor', 'port')),
	'kapacitor_database':utils.chop_comment(config_file.get('kapacitor', 'database')),
	'kapacitor_measurement':utils.chop_comment(config_file.get('kapacitor', 'measurement')),
	'kapacitor_username':utils.chop_comment(config_file.get('kapacitor', 'username')),
	'kapacitor_password':utils.chop_comment(config_file.get('kapacitor', 'password')),
	'kapacitor_notifications':utils.chop_comment(config_file.get('kapacitor', 'notifications')),
	'http_ip':utils.chop_comment(config_file.get('http', 'bind_address')),
	'http_port':int(utils.chop_comment(config_file.get('http', 'port'))),
	'interrogator_interval':int(utils.chop_comment(config_file.get('timers', 'interrogator'))),
	'mqtt_publish_interval':int(utils.chop_comment(config_file.get('timers', 'publish_interval'))),
	'xbee_max_sleep':int(utils.chop_comment(config_file.get('timers', 'xbee_max_sleep'))),
	'interrogator_enabled':config_file.getboolean('diagnostic_commands', 'enabled'),
	'local_diag_commands':config_file.get('diagnostic_commands', 'coordinator').split(','),
	'remote_diag_commands':config_file.get('diagnostic_commands', 'end_devices').split(','),
	'timezone':config_file.get('environment', 'timezone'),
	'latitude':config_file.get('environment', 'latitude'),
	'longitude':config_file.get('environment', 'longitude'),
	'openweathermap_city':config_file.getint('openweathermap', 'city'),
	'openweathermap_appid':config_file.get('openweathermap', 'appid'),
	'openweather_interval':int(utils.chop_comment(config_file.get('timers', 'openweather_refresh'))),
	'openweather_enabled':config_file.getboolean('openweathermap', 'enabled')
	})
	transceivers = {}
	for section in config_file.sections():
		if section.startswith('0013a2'):
			for key, value in config_file.items(section):
				if key == 'transceiver_id':
					transceiver_id = value
					transceivers.update({transceiver_id:{'settings':{'mac':section}}})
					for key, value in config_file.items(section):
						if key == 'transceiver_id':
							continue
						else:
							value = utils.chop_comment(value)
							transceivers[transceiver_id]['settings'].update({key:value})
					transceivers[transceiver_id].update({'status':{'frames':{'rx':0,'tx':0},'responses':{'0':0, '1':0, '2':0, '3':0, '4':0}}})
					rr_database.update({transceiver_id:{}})
	# Retrieve MQTT gateways 
	mqtt_gateways = {}
	for section in config_file.sections():
		for key, value in config_file.items(section):
			if key == 'protocol' and value == 'mqtt':
				# this is a mqtt trancvers
				for key, value in config_file.items(section):
					if key == 'gateway_id':
						gateway_id = value
						mqtt_gateways.update({gateway_id:{'settings':{'mac':section}}})
					for key, value in config_file.items(section):
						if key == 'gateway_id':
							continue
						else:
							value = utils.chop_comment(value)
							mqtt_gateways[gateway_id]['settings'].update({key:value})
	stats = {'frames':{'last':'n/a'}}
	at_commands = {
	'1':{
		'Diagnostics':{
			'VR':'Firmware Version',
			'HV':'Hardware Version',
			'DB':'Received Signal Strength',
			'AI':'Association Indication',
			'EC':'CCA Failures',
			'EA':'ACK Failures'},
		'Networking':{
			'ID':'PAN ID',
			'CH':'Operating Channel',
			'NI':'Node Identifier'},
		'I/O Settings':{
			'D0':'DIO0 Configuration',
			'D1':'DIO1 Configuration',
			'D2':'DIO2 Configuration',
			'D3':'DIO3 Configuration',
			'D4':'DIO4 Configuration',
			'D5':'DIO5 Configuration',
			'D6':'DIO6 Configuration',
			'D7':'DIO7 Configuration',
			'D8':'DIO8 Configuration',
			'IT':'Samples before TX',
			'IC':'DIO Change Detect',
			'IR':'Sample Rate',
			'P0':'PWM0 Configuration',
			'P1':'PWM1 Configuration',
			'PT':'PWM Output Timeout',
			'RP':'RSSI PWM Timer'},
		'Sleep Modes':{
			'SM':'Sleep Mode',
			'SN':'Number of Cyclic Sleep Periods',
			'ST':'Time before Sleep',
			'SP':'Cyclic Sleep Period',
			'DP':'Disassociated Cyclic Sleep Period',
			'SO':'Sleep Options',
			'PO':'Poll Rate'},
		'RF Interfacing':{
			'PL':'Power Level',
			'PM':'Power Mode',
			'PP':'Power at PL4'},
		'Security':{
			'EE':'Encryption Enable',
			'EO':'Encryption Options'},
		'Addressing':{
			'SH':'Destination Address High',
			'DL':'Destination Address Low',
			'MY':'16-bit Network Address',
			'SH':'Serial Number High',
			'SL':'Serial Number Low',
			'NI':'Node Identifier'}
		},
	'2':{
		'Networking':{
			'ID':'PAN ID',
			'OP':'Oerating PAN ID',
			'OI':'Operating 16-bit PAN ID',
			'CH':'Operating Channel'},
		'I/O Settings':{
			'D0':'DIO0 Configuration',
			'D1':'DIO1 Configuration',
			'D2':'DIO2 Configuration',
			'D3':'DIO3 Configuration',
			'D4':'DIO4 Configuration',
			'D5':'DIO5 Configuration',
			'D6':'DIO6 Configuration',
			'D7':'DIO7 Configuration',
			'IC':'DIO Change Detect',
			'IR':'Sample Rate',
			'P0':'DIO10 Configuration',
			'P1':'DIO11 Configuration',
			'P2':'DIO12 Configuration',
			'PT':'PWM Output Timeout',
			'RP':'RSSI PWM Timer'},
		'Diagnostics':{
			'TP':'Temperature',
			'VR':'Firmware Version',
			'HV':'Hardware Version',
			'DB':'Received Signal Strength',
			'AI':'Association Indication',
			'EA':'ACK Failures',
			'%V':'Supply Voltage'
			},
		'Sleep Modes':{
			'SM':'Sleep Mode',
			'SN':'Number of Cyclic Sleep Periods',
			'ST':'Time before Sleep',
			'SP':'Cyclic Sleep Period',
			'DP':'Disassociated Cyclic Sleep Period',
			'SO':'Sleep Options',
			'PO':'Poll Rate'},
		'RF Interfacing':{
			'PL':'Power Level',
			'PM':'Power Mode',
			'PP':'Power at PL4'},
		'Security':{
			'EE':'Encryption Enable',
			'EO':'Encryption Options'},
		'Addressing':{
			'SH':'Destination Address High',
			'DL':'Destination Address Low',
			'MY':'16-bit Network Address',
			'SH':'Serial Number High',
			'SL':'Serial Number Low',
			'NI':'Node Identifier'}
		}
	}

def transceiver_load():
	global transceiver_rfs
	transceiver_rfs = {}
	for transceiver, attributes in transceivers.iteritems():
		for parameter, value in attributes['settings'].iteritems():
			transceiver_rfs.update({transceiver:{}})
			if parameter == 'protocol' and value == 'raw':
				ser = serial.Serial(attributes['settings']['port'], attributes['settings']['baudrate'], timeout = 4)
			elif parameter == 'protocol' and value == 'rfc2217':
				ser = serial.serial_for_url(attributes['settings']['port'], attributes['settings']['baudrate'], timeout = 4)
		for parameter, value in attributes['settings'].iteritems():
			if parameter == 'series' and value == '1':
				xz_object = XBee(ser)
			elif parameter == 'series' and value == '2':
				xz_object = ZigBee(ser)
			series = attributes['settings']['series']
		transceiver_rfs.update({transceiver:{'xbee_object':xz_object, 'serial':ser, 'series':series, 'transceiver_id':transceiver}})

### HTTP server classes here
class HTTPRequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.table_attributes = {}
		http_stats = {}
		#radio_db_dict = radio_db_load('dict')
		http_stats.update({'devices':ed_status})
		http_stats.update({'transceivers':transceivers})
		http_stats.update({'queue':queue})
		http_json = json.dumps(http_stats).replace("false", "0").replace("true", "1")
		self.wfile.write(json2html.convert(json = http_json))
		#else:
		#	self.send_response(403)
		#	self.send_header('Content-Type', 'application/json')
		#	self.end_headers()
		return

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
	allow_reuse_address = True
	def shutdown(self):
		self.socket.close()
		HTTPServer.shutdown(self)

class http_server():
	def __init__(self, ip, port):
		self.server = ThreadedHTTPServer((ip, port), HTTPRequestHandler)
	def start(self):
		self.server_thread = threading.Thread(target=self.server.serve_forever, name = 'http_thread')
		self.server_thread.daemon = True
		self.server_thread.start()
	def waitForThread(self):
		self.server_thread.join()
	def stop(self):
		self.server.shutdown()
		self.waitForThread()

### MQTT funcionts here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Succesfully connected to MQTT broker, starting subscriptions if any.")
		# Start Subscriptions here
		client.subscribe(config['mqtt_subscribe_topic'], 1)
		logger.warning("Subscribed to topic '%s'" % config['mqtt_subscribe_topic'])
	else:
		logger.warning("Connection to MQTT broker failed. rc='%s' " % s)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH '%s' completed." % mid)

def on_message(client, userdata, msg):
	if msg.topic == config['mqtt_subscribe_topic']:
		logger.info("Processing Publish '%s'." % msg.payload)
		# First convert payload to a dictionary
		request = ast.literal_eval(msg.payload)
		# Then extract deviceid from the messsage
		for key, value in request.iteritems():
			if key == 'deviceid':
				deviceid = value
				# Check if the deviceid is known in the radio_db config file
				for device, settings in radio_db_dict.iteritems():
					if deviceid in settings.itervalues():
						# If device exists we need to inspect the whole message again
						for key, value in request.iteritems():
							if key in ['gpio', 'command']:
								thread_name = str(deviceid)+"_at_request"
								thread = send_xbee_at(name=thread_name, kwargs=request)
								thread.start()
								time.sleep(0.05)

def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

def publish(payload):
	if config['influxdb_enabled']:
		if 'id' not in payload:
			publish_influxdb(payload)
	if config['elasticsearch_enabled']:
		#for now only GPS coordinates are supported
		if 'latitude' in payload:
			elasticsearch_influxdb(payload)
	if config['mqtt_enabled']:
		publish_mqtt(payload)

def elasticsearch_influxdb(payload):
	from datetime import datetime
	now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
	json_body = {
			"date": now,
			"deviceid": payload['deviceid'],
			"location": payload['location'],
			"sublocation": payload['sublocation'],
			"battery": payload['battery_level'],
			"rssi": payload['rssi'],
			"temperature": payload['temperature'],
			"coordinates": [ float(payload['longitude']), float(payload['latitude']) ]
		}
	if runmode == 'debug':
		print "# Elasticsearch payload: '%s'" % json_body
	res = es.index(index = 'iot-gps', doc_type='device', body = json_body)

	if runmode == 'debug':
		print "# Index created: %s" % res

def publish_influxdb(payload):
	from datetime import datetime
	now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
	fields = {}
	# Extract fields from payload
	for key, value in payload.iteritems():
		if key == 'deviceid':
			deviceid = value
		elif key == 'sublocation':
			sublocation = value
		elif key == 'location':
			location = value
		elif key =='gpios':
			continue
		else:
			fields.update({key:float(value)})
	json_body = {
			"measurement": "xqtt-measurements",
			"tags": {
				"location": location,
				"sublocation": sublocation,
				"deviceid": deviceid
				},
	}
	json_body.update({'fields':fields, 'time':now})
	json_body = [json_body]
	if runmode == 'debug':
		print "# InfluxDB payload: '%s'" % json_body
	if fields:
		influxdbc.write_points(json_body)

def publish_mqtt(payload):
	from datetime import datetime
	### Add timestamp
	now = datetime.now(pytz.timezone(config['timezone'])).strftime('%Y-%m-%d %H:%M:%S')
	payload.update({'ts':now})
	### Get publish format
	if config['mqtt_publish_format'] == 'json' or config['mqtt_publish_format'] == 'both':
		# Pass the container to the MQTT thread for publishing
		mqttc.publish(config['mqtt_publish_topic'], payload=json.dumps(payload))
		if runmode == 'debug':
			print "# MQTT: '%s'" % config['mqtt_publish_topic']+str(payload)
	payload.pop('ts')
	if config['mqtt_publish_format'] == 'plain' or config['mqtt_publish_format'] == 'both':
		topic = config['mqtt_publish_topic'][:-1]
		location = payload['location']
		deviceid = payload['deviceid']
		sublocation = payload['sublocation']
		for sensor, readout in payload.iteritems():
			if sensor not in ["deviceid", "location", "sublocation"] and sensor is not 'gpios':
				mqttc.publish('%s/%s/%s/%s/%s' % (topic, location, sublocation, deviceid, sensor), readout)
				if runmode == 'debug':
					print "# MQTT: '%s/%s/%s/%s/%s/%s' " % (topic, location, sublocation, deviceid, sensor, readout)
			elif sensor == 'gpios':
				for g, gpio in readout.iteritems():
					mqttc.publish('%s/%s/%s/%s/%s' % (topic, location, sublocation, deviceid, g), gpio)
					if runmode == 'debug':
						print "# MQTT: '%s/%s/%s/%s/%s/%s' " % (topic, location, sublocation, deviceid, g, gpio)

	if runmode == 'debug':
		print "##### END #####"

### Classes / functions start here
class mqtt_connect(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		global mqttc
		mqttc = mqtt.Client(config['hostid'])
		mqttc.username_pw_set(config['mqtt_pass'], config['mqtt_pass'])
		mqttc.connect(config['mqtt_hostname'], config['mqtt_port'], 80)
		mqttc.on_connect = on_connect
		mqttc.on_message = on_message
		mqttc.on_publish = on_publish
		mqttc.on_disconnect = on_disconnect
		# Uncomment here to enable MQTT debug messages
		#mqttc.loop_start()
		while True:
			mqttc.loop()

def influxdb_connect():
	from influxdb import InfluxDBClient
	global influxdbc
	influxdbc = InfluxDBClient(config['influxdb_hostname'], config['influxdb_port'], config['influxdb_username'], config['influxdb_password'], config['influxdb_dbname'])
	logger.warning("Creating influxdb client object.")

def elasticsearch_connect():
	from elasticsearch import Elasticsearch
	global es
	es = Elasticsearch([{'host':config['elasticsearch_hostname'],'port':config['elasticsearch_port']}])
	if runmode == 'debug':
		print es.info()
	logger.warning("Creating elasticsearch client object.")

### This thread maintains envriomental variables and restarts program's threads in case they crash for any reason
class supervisor(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		while True:
			try:
				# Start MQTT thread if none running
				if "mqtt_thread" not in get_threads():
					thread = mqtt_connect(name = 'mqtt_thread')
					thread.daemon = True
					thread.start()
					logger.warning("Restarting mqtt_connect thread...")
				# Web server thread here if none running
				elif "http_thread" not in get_threads():
					thread = http_server(config['http_ip'], config['http_port'])
					thread.start()
					logger.warning("Restarting http_server thread...")
				# Start read_xbee_frame threads if not already running, one per xbee modem configured
				for transceiver, attributes in transceiver_rfs.iteritems():
					if str(transceiver)+"_dispatcher_thread" not in get_threads():
						thread_name = str(transceiver)+"_dispatcher_thread"
						thread = dispatcher(config['mqtt_publish_interval'], thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Restarted dead dispatcher thread for xbee transceiver '%s' ..." % transceiver)
					elif str(transceiver)+"_coordinator_thread" not in get_threads():
						thread_name = str(transceiver)+"_coordinator_thread"
						thread = read_xbee_frame(attributes['xbee_object'],  attributes['series'], attributes['transceiver_id'], name = thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Restarted dead read_xbee_frame thread for xbee transceiver '%s' ..." % transceiver)
					elif "publish_openweather" not in get_threads() and config['openweather_enabled']:
						thread = publish_openweather(name = 'publish_openweather', interval = config['openweather_interval'])
						thread.daemon = True
						thread.start()
						logger.warning("Restarted dead publish_openweather thread...")
					elif str(transceiver)+"_interrogator_thread" not in get_threads() and config['interrogator_enabled']:
						thread_name = str(transceiver)+"_interrogator_thread"
						thread = interrogator(attributes['xbee_object'], attributes['transceiver_id'], config['interrogator_interval'], thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Restarted dead interrogator thread for xbee transceiver '%s' ..." % xbee)
				# Restart read_gateway_frame threads if not already running, one per gateway
				for gateway, attributes in mqtt_gateways.iteritems():
					if str(gateway)+"_gateway_thread" not in get_threads():
						thread_name = str(gateway)+"_gateway_thread"
						thread = read_gateway_frame(gateway, thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Restarted a read_gateway_frame thread for gateway '%s' ..." % gateway)
				time.sleep(1)
			except Exception:
				sys.excepthook(*sys.exc_info())
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print "Other error or exception occurred!"

class read_xbee_frame(threading.Thread):
	def __init__(self, xbee, series, transceiver_id, name = None):
		threading.Thread.__init__(self, name = name)
		self.xbee = xbee
		self.series = series
		self.transceiver_id = transceiver_id
		self.xbee.send('at', frame_id='A', command='FR')
		return
	def run(self):
		while True:
			try:
				### Initiate containers for MQTT payload here
				frame_payload = {}
				payload = {}
				gpio_payload = {}
				### Get frame from xbee objects
				frame = self.xbee.wait_read_frame()
				# Update transceivers stats dict 
				transceivers[self.transceiver_id]['status']['frames']['rx'] += 1 
				### For debugging porpuses we print all frames when running in debug mode
				if runmode == 'debug':
					print "##### ##### DEBUG %s ##### ##### #####" % self.name
					print "# Frame: '%s'" % frame
				### Frame processing starts here
				# Get rssi if in frame
				if ('rssi' in frame):
					rssi = utils.get_rssi(frame['rssi'])
					payload.update({'rssi':rssi})
					if runmode == 'debug':
						print "# RSSI: '%s'" % rssi
				# Status frame is sent to a learner thread
				if 'status' in frame:
					logger.info("Received status frame '%s', launching a learner thread for further processing..." % frame)
					thread = learner(frame, self.transceiver_id)
					thread.start()
					continue
				elif ('source_addr_long' in frame and 'samples' in frame) and self.series == '1':
					rssi = utils.get_rssi(frame['rssi'])
					payload.update({'rssi':rssi})
				elif ('source_addr_long' in frame and 'samples' in frame) and self.series == '2':
					### Convert frame fields source_addr_long and source_addr to hex strings
					frame['source_addr'] = utils.bytetohex(frame['source_addr'])
				elif ('source_addr_long' not in frame and 'source_addr' in frame):
					frame['source_addr_long'] = frame['source_addr']
				#else:
				#	print "Unsupported frame received."
				#	continue
				### Convert frame field source_addr from bytes to hex string
				
				frame['source_addr_long'] = ''.join("{:02X}".format(ord(c)) for c in frame['source_addr_long']).lower()
				if runmode == 'debug':
					print "# Sender: '%s'" % frame['source_addr_long']
				# See if  source address long exists in radio_db config file
				if not radio_db.has_option(frame['source_addr_long'], 'id'):
					logger.exception("Missing device definition in radio_db config file!, offending frame: '%s'" % frame)
					continue
				deviceid = radio_db.get(frame['source_addr_long'], 'id')
				location = radio_db.get(frame['source_addr_long'], 'location')
				# Get sublocation from deviceid or sensor
				sublocation = sublocation = radio_db.get(frame['source_addr_long'], 'sublocation')
				# If checks above pass prepare containers for MQTT base payload here
				base_payload = {'deviceid':deviceid, 'location':location, 'sublocation':sublocation}
				# Decode frame payload
				frame_payload = utils.decode_payload(frame, radio_db)
				if len(frame_payload) >= 1:
					payload.update(base_payload)
					payload.update(frame_payload)
					publish(payload)
					# Clear the payload dict for next section
					payload.clear()
				### Digital sensors processing starts here
				if frame['id'] == 'rx_io_data_long_addr':
					for dio, value in frame['samples'][0].iteritems():
						# Get dio number and prepend D to it
						dio = "D" + str(int(filter(str.isdigit, dio)))
						# Check if this is a dio only frame
						dio_only = utils.is_dio_only(frame['samples'][0])
						if not radio_db.has_option(frame['source_addr_long'], dio) and not value:
							# Ignore this readout since sensor is not configured and value is False
							continue
						elif not radio_db.has_option(frame['source_addr_long'], dio):
							logger.exception("Missing sensor definition for line '%s', device '%s' in radio_db config file!." % (dio, deviceid))
							continue
						sensor = radio_db.get(frame['source_addr_long'], dio)
						if dio_only:
							base_payload['sublocation'] = utils.get_sublocation(sensor, sublocation)
							payload.update(base_payload)
							if sensor[:3] == "mic" and value == False: # false means sound detected, true means silence
								frame_payload.update({sensor:1})
							elif sensor[:4] == "rain" and value == False: # false means rain
								frame_payload.update({'rain':1})
							elif sensor[:3] == "pir" and value == True: # True means pir detected, False means no pir
								frame_payload.update({'presence':1, 'pir':1})
							elif sensor[:3] == "pir" and value == False: # True means pir detected, False means no pir
								frame_payload.update({'presence':0, 'pir':0})
							#elif sensor == "vibration" and value == True: # true means motion
							#	frame_payload.update({'vib':1})
							# Else skip this readout since the sensor is not supported 
							else:  
								continue
							# Publish what we have before next iteration
							if 1 in frame_payload.values():
								payload.update(frame_payload)
								publish(payload)
								# Clear the frame_payload dict for next iteration
								frame_payload.clear()
						# If the frame comes with ADC and DIO we get the DIO devices for publishing 
						else:
							if sensor[:3] in ["buz", "pir", "led"] or sensor[:5] in ["relay"]:
								gpio_payload.update({dio:sensor})
					if len(gpio_payload) >= 1:
						payload.update(base_payload)
						payload.update({'gpios':gpio_payload})
						publish(payload)
						# Clear the payload dict for next iteration
						payload.clear()
						### Update stats here, not fully implemented
					stats['frames'].update({'last':frame})
			except Exception:
				sys.excepthook(*sys.exc_info())
				break
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print "Other error or exception occurred!"

class read_gateway_frame(threading.Thread):
	def __init__(self, gateway_id, name):
		threading.Thread.__init__(self, name = name)
		self.gateway_id = gateway_id
		self.hostname = mqtt_gateways[self.gateway_id]['settings']['hostname']
		self.port = int(mqtt_gateways[self.gateway_id]['settings']['port'])
		self.username = mqtt_gateways[self.gateway_id]['settings']['username']
		self.password = mqtt_gateways[self.gateway_id]['settings']['password']
		self.type = mqtt_gateways[self.gateway_id]['settings']['type']
		if self.type == 'rfm69':
			self.topic = mqtt_gateways[self.gateway_id]['settings']['topic']+'/'+mqtt_gateways[self.gateway_id]['settings']['mac']+'/#'
		elif self.type == 'lorawan':
			self.topic = mqtt_gateways[self.gateway_id]['settings']['topic']+'/#'
		return
	def run(self):
		mc = mqtt.Client()
		mc.username_pw_set(self.username, self.password)
		mc.connect(self.hostname, self.port,  80)
		mc.on_connect = self.on_connect
		mc.on_message = self.on_message
		#mc.on_publish = on_publish
		mc.on_disconnect = self.on_disconnect
		#mc.loop_start()
		# Uncomment here to enable MQTT debug messages
		#mc.on_log = on_log
		while True:
			mc.loop()

	def on_message(self, client, userdata, msg):
		logger.info("Processing MQTT Publish from gateway '%s'." % msg.payload)
		# First convert payload to a dictionary
		if runmode == 'debug':
			print "##### ##### DEBUG %s ##### ##### #####" % self.name
			print "# Frame: '%s'" % msg.payload
		### Frame processing starts here
		frame = json.loads(msg.payload)
		if {'devEUI', 'object'} <= set(frame):
			# Then extract device identification and payload messsage
			if not radio_db.has_option(frame['devEUI'], 'id'):
				logger.exception("Missing device definition in radio_db config file!, offending frame: '%s'" % frame)
			else:
				deviceid = radio_db.get(frame['devEUI'], 'id')
				location = radio_db.get(frame['devEUI'], 'location')
				sublocation = sublocation = radio_db.get(frame['devEUI'], 'sublocation')
				rssi = frame['rxInfo'][0]['rssi']
				payload = utils.decode_gateway_payload(frame['object'], config)
				payload.update({'deviceid':deviceid, 'rssi':rssi, 'location':location, 'sublocation': sublocation})
				# Publish payload
				publish(payload)

	def on_connect(self, client, userdata, flags, rc):
		if rc == 0:
			logger.warning("Succesfully connected to MQTT gateway '%s', starting subscriptions." % self.gateway_id)
			# Start Subscriptions here
			client.subscribe(self.topic, 1)
			logger.warning("Subscribed to MQTT gateway '%s'" % self.topic)
		else:
			logger.warning("Connection MQTT gateway failed. rc='%s' " % s)

	def on_disconnect(self, client, userdata, rc):
		if rc != 0:
			logger.warning("Unexpected MQTT gateway '%s' disconnection. Will auto-reconnect." % self.gateway_id)


def reset_xbee(transceiver):
	try:
		logger.warning("Soft resetting transceiver '%s'." % transceiver)
		xbee = transceiver_rfs[transceiver]['xbee_object']
		# Send local reset AT command
		xbee.send('at', frame_id='A', command='FR')
		# Wait for response
		#response = xbee.wait_read_frame()
        #time.sleep(0.05)
	except:
		logger.exception("Reset local Xbee failed!")

class send_xbee_at(threading.Thread):
	def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
		threading.Thread.__init__(self, group=group, target=target, name=name, verbose=verbose)
		self.args = args
		self.kwargs = kwargs
		self.timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
		return
	def run(self):
		# Begin request process, get destination address and coordinator object
		for key, value in self.kwargs.iteritems():
			if key == 'deviceid':
				deviceid = value
				# Get long addr for this device
				for address, settings in radio_db_dict.iteritems():
					if deviceid in settings.itervalues():
						dest_addr_long = address
						transceiver_id = settings['transceiver_id']
			elif key == 'gpio':
				gpio = value
			elif key == 'status':
				status = value
			elif key == 'command':
				command = value.upper()
			elif key == 'parameter':
				parameter = value
			else:
				continue
		# Geenerate frame_id request id and make sure it does not already exist in the rr databaser 
		frame_id = utils.bytetoint(utils.randomid(1))
		while frame_id in rr_database[transceiver_id]:
			frame_id = utils.bytetoint(utils.randomid(1))
		request_id = utils.randomid(3)
		while request_id in queue:
			request_id = utils.randomid(3)
		timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
		if 'gpio' in locals():
			# Map values for on / off depending on sensor type
			parameter = utils.onoff(gpio, status)
			# Priority 0 which is the highest
			priority = 0
			# Time to live 5 secs
			ttl = 5
			# Status list 0 = OK, 1 = ERR , 2 = Invalid command, 3 = Invalid parameter , 4 = Tx Failure, 5 = Pending delivery
			status = 5
			# Responses for this request are not to be published over MQTT
			publish = False
			# Now get dio number lines using device address
			for key, value in radio_db_dict[dest_addr_long].iteritems():
				if value == gpio:
					command = key
					if command == 'd11':
						command = 'p1'
					elif command == 'd12':
						command = 'p2'
		elif 'command' in locals():
			if 'parameter' not in locals():
				parameter = None
			publish = True
			priority = 1
			ttl = config['xbee_max_sleep']
			status = 5
		queue.update({request_id:{'priority':priority, 'last_retry':timestamp, 'retry':0, 'at':'remote', 'status':status, 'timestamp':timestamp, 'ttl':ttl, 'deviceid':deviceid, 'dest_addr_long':dest_addr_long, 'transceiver_id':transceiver_id, 'frame_id':frame_id, 'command':command, 'parameter':parameter}})
		rr_database[transceiver_id].update({frame_id:{'deviceid':deviceid, 'command':command, 'parameter':parameter, 'transceiver_id':transceiver_id,  'publish':publish, 'timestamp':timestamp}})
		priority_queue.put({request_id:{'priority':priority, 'last_retry':timestamp, 'retry':0, 'at':'remote', 'status':status, 'timestamp':timestamp, 'ttl':ttl, 'deviceid':deviceid, 'dest_addr_long':dest_addr_long, 'transceiver_id':transceiver_id, 'frame_id':frame_id, 'command':command, 'parameter':parameter}}, priority)

### This thread handles delivery of remote and local at commands to end devices at time intervals
class dispatcher(threading.Thread):
	def __init__(self, interval, name):
		threading.Thread.__init__(self, name = name)
		self.interval = interval
		return
	def run(self):
		while True:
			try:
				request = priority_queue.get()
				for request_id, message in request.iteritems():
					priority = message['priority']
					command = message['command']
					parameter = message['parameter']
					if parameter is not None:
						parameter = chr(int(parameter))
					frame_id = bytes(bytearray([message['frame_id']]))
					deviceid = message['deviceid']
					status = message['status']
					ttl = message['ttl']
					retry = message['retry']
					last_retry = datetime.datetime.strptime(message['last_retry'], '%Y-%m-%d %H:%M:%S.%f')
					timestamp = datetime.datetime.strptime(message['timestamp'], '%Y-%m-%d %H:%M:%S.%f')
					delapsed = (last_retry - timestamp).seconds
					#delapsed = (last_retry - timestamp).total_seconds() * 1000
					now = datetime.datetime.now()
					# Now get the coordinator for this end device get the xbee_object associated to it
					transceiver_id = message['transceiver_id']
					for transceiver, attributes in transceiver_rfs.iteritems():
						if transceiver_id in attributes.itervalues():
							xbee_object = attributes['xbee_object']
					if message['at'] == 'remote':
						# Calculate long destination address and convert it to a byte array
						dest_addr_long = [r"0x" + message['dest_addr_long'][i:i+2] for i in range(0, len(message['dest_addr_long']), 2)]
						dest_addr_long = bytearray( int(x,16) for x in dest_addr_long)
						# Push this command to the coordinator if status 5 pending delivery or 4 tx failure
						if status in [5, 4]:
							# Priority 0 get delivered first
							if priority == 0:
								max_retry = 3
								if retry < max_retry:
									xbee_object.remote_at(dest_addr_long = dest_addr_long, options='\x02', command = command, parameter = parameter, frame_id = frame_id)
									queue[request_id]['retry'] += 1
									# Update transceivers stats dict 
									transceivers[transceiver_id]['status']['frames']['tx'] += 1
									if runmode == 'debug':
										print "##### ##### DEBUG %s ##### ##### #####" % self.name
										print "# Request ID '%s' Frame ID '%s' with '%s'='%s' to '%s' at '%s' via transceiver_id '%s'" % (request_id, utils.bytetoint(frame_id), command, utils.bytetoint(parameter), deviceid, utils.bytetohex(dest_addr_long), transceiver_id)
										print "##### END #####"
							else:
								# Remote AT commands retry 10 secs for 3 times
								if priority == 1:
									retry_interval = 15
									max_retry = 3
								# Diag commands retry every 20 secs for 3 times 
								elif priority == 2:
									retry_interval = 60
									max_retry = 1
								if delapsed >= retry_interval and retry <= max_retry:
									xbee_object.remote_at(dest_addr_long = dest_addr_long, options='\x02', command = command, parameter = parameter, frame_id = frame_id)
									# Update transceivers stats dict 
									transceivers[transceiver_id]['status']['frames']['tx'] += 1 
									if runmode == 'debug':
										print "##### ##### DEBUG %s ##### ##### #####" % self.name
										print "# Request ID '%s' Frame ID '%s' with '%s'='%s' to '%s' at '%s' via transceiver_id '%s'" % (request_id, utils.bytetoint(frame_id), command, utils.bytetoint(parameter), deviceid, utils.bytetohex(dest_addr_long), transceiver_id)
										print "##### END #####"
								# Send this request back to the queue
								else:
									queue[request_id].update({'last_retry':now.strftime('%Y-%m-%d %H:%M:%S.%f')})
									message.update({'last_retry':now.strftime('%Y-%m-%d %H:%M:%S.%f')})
									request = {request_id:message}
									priority_queue.put(request, priority)
					elif message['at'] == 'local':
						if priority == 1 and status in [5, 4]:
							retry_interval = 3
							max_retry = 3
							if delapsed >= retry_interval and retry <= max_retry:
								xbee_object.at(command = command, frame_id = frame_id)
								# Update transceivers stats dict 
								transceivers[transceiver_id]['status']['frames']['tx'] += 1 
								if runmode == 'debug':
									print "##### ##### DEBUG %s ##### ##### #####" % self.name
									print "# Local AT: Sent frame id '%s' with command='%s' to transceiver_id '%s'" % (utils.bytetoint(frame_id), command, transceiver_id)
									print "##### END #####"
							# Send this request back to the queue
							else:
								queue[request_id].update({'last_retry':now.strftime('%Y-%m-%d %H:%M:%S.%f')})
								message.update({'last_retry':now.strftime('%Y-%m-%d %H:%M:%S.%f')})
								request = {request_id:message}
								priority_queue.put(request, priority)
				# Cleanup, remove expired messages present in the queue database
				tempq = queue.copy()
				for request_id, message in tempq.iteritems():
					timestamp = datetime.datetime.strptime(message['timestamp'], '%Y-%m-%d %H:%M:%S.%f')
					now = datetime.datetime.now()
					elapsed = (now - timestamp).seconds
					priority = message['priority']
					frame_id = message['frame_id']
					ttl = message['ttl']
					transceiver_id = message['transceiver_id']
					status = message['status']
					if elapsed >= (ttl + 5) or status == 0:
						if runmode == 'debug':
							print "##### ##### DEBUG %s ##### ##### #####" % self.name
							print "# Request ID: '%s' with '%s' secs old has exceeeded its time to live or was successfully delivered thus removing from queue and request / response databases." % (request_id, elapsed)
						del queue[request_id]
						del rr_database[transceiver_id][frame_id]
				time.sleep(0.05)
			except Exception:
				sys.excepthook(*sys.exc_info())

### This thread interrogates end devices at time intervals
class interrogator(threading.Thread):
	def __init__(self, xbee, transceiver_id, interval, name):
		threading.Thread.__init__(self, name = name)
		self.interval = interval
		self.commands = at_commands
		self.xbee = xbee
		self.transceiver_id = transceiver_id
		return
	def run(self):
		while True:
			try:
				# Prepare variables to put this request in the queue 
				status = 5
				parameter = None
				# Local diags gathered first 
				for command in config['local_diag_commands']:
					deviceid = self.transceiver_id
					ttl = 5
					priority = 1
					dest_addr_long = None
					timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
					# Geenerate frame_id request id and make sure it does not already exist in the rr databaser 
					frame_id = utils.bytetoint(utils.randomid(1))
					while frame_id in rr_database[self.transceiver_id]:
						frame_id = utils.bytetoint(utils.randomid(1))
					request_id = utils.randomid(3)
					while request_id in queue:
						request_id = utils.randomid(3)
					# Wrap up request and add it to queue database
					queue.update({request_id:{'priority':priority, 'last_retry':timestamp, 'retry':0, 'at':'local', 'status':status, 'timestamp':timestamp, 'ttl':ttl, 'deviceid':deviceid, 'dest_addr_long':dest_addr_long, 'transceiver_id':self.transceiver_id, 'frame_id':frame_id, 'command':command, 'parameter':parameter}})
					rr_database[self.transceiver_id].update({frame_id:{'deviceid':deviceid, 'command':command, 'parameter':parameter, 'transceiver_id':self.transceiver_id,  'publish':True, 'timestamp':timestamp}})
					priority_queue.put({request_id:{'priority':priority, 'last_retry':timestamp, 'retry':0, 'at':'local', 'status':status, 'timestamp':timestamp, 'ttl':ttl, 'deviceid':deviceid, 'dest_addr_long':dest_addr_long, 'transceiver_id':self.transceiver_id, 'frame_id':frame_id, 'command':command, 'parameter':parameter}}, priority)
					time.sleep(1)
				# End devices diags gathered priority 2 and 60 secs ttl
				for command in config['remote_diag_commands']:
					ttl = config['xbee_max_sleep']
					priority = 2
					timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
					# Generate frame_id request id and make sure it does not already exist in the rr databaser 
					frame_id = utils.bytetoint(utils.randomid(1))
					while frame_id in rr_database[self.transceiver_id]:
						frame_id = utils.bytetoint(utils.randomid(1))
					request_id = utils.randomid(3)
					while request_id in queue:
						request_id = utils.randomid(3)
					# Get list of configured xbees and their associated coordinators 
					for address, settings in radio_db_dict.iteritems():
						# Grab the end devices associated to this coordinator
						if self.transceiver_id in settings.itervalues():
							if self.transceiver_id == settings['id']:
								continue
							else:
								# Calculate long destination address and convert it to a byte array
								dest_addr_long = address
								deviceid = settings['id']
							# Wrap up request and push it to queue and delivert databases
							queue.update({request_id:{'priority':priority, 'last_retry':timestamp, 'retry':0, 'at':'remote', 'status':status, 'timestamp':timestamp, 'ttl':ttl, 'deviceid':deviceid, 'dest_addr_long':dest_addr_long, 'transceiver_id':self.transceiver_id, 'frame_id':frame_id, 'command':command, 'parameter':parameter}})
							rr_database[self.transceiver_id].update({frame_id:{'deviceid':deviceid, 'command':command, 'parameter':parameter, 'transceiver_id':self.transceiver_id,  'publish':True, 'timestamp':timestamp}})
							priority_queue.put({request_id:{'priority':priority, 'last_retry':timestamp, 'retry':0, 'at':'remote', 'status':status, 'timestamp':timestamp, 'ttl':ttl, 'deviceid':deviceid, 'dest_addr_long':dest_addr_long, 'transceiver_id':self.transceiver_id, 'frame_id':frame_id, 'command':command, 'parameter':parameter}}, priority)
							time.sleep(5)
				time.sleep(self.interval)
			except Exception:
				sys.excepthook(*sys.exc_info())

### Process status frames
class learner(threading.Thread):
	def __init__(self, message, transceiver_id):
		threading.Thread.__init__(self)
		self.message = message
		self.commands = at_commands
		self.transceiver_id = transceiver_id
		self.now = datetime.datetime.now()
		return
	def run(self):
		try:
			# Extract relevant fields 
			frame_id = utils.bytetoint(self.message['frame_id'])
			status = utils.bytetohex(self.message['status'])
			if status == '0':
				# If status is 0 it means end device ACK'd a request therefore we update status of this request from pending delivery 5 to delivered ok 0 to prevent retransmissions
				q4 = queue.copy()
				for request_id, message in q4.iteritems():
					if frame_id in message.itervalues() and self.transceiver_id in message.itervalues():
						queue[request_id]['status'] = 0
						if 'parameter' not in self.message:
							 continue
						# For responses coming from end devices get relevant parameterss radio_db file and convert frame field source_addr from bytes to hex strings
						if self.message['id'] == 'remote_at_response':
							mac = ''.join("{:02X}".format(ord(c)) for c in self.message['source_addr_long']).lower()
							deviceid = radio_db.get(mac, 'id')
							location = radio_db.get(mac, 'location')
							sublocation =  radio_db.get(mac, 'sublocation')
						elif self.message['id'] == 'at_response':
							deviceid = radio_db.get(self.transceiver_id, 'id')
							location = radio_db.get(self.transceiver_id, 'location')
							sublocation =  radio_db.get(self.transceiver_id, 'sublocation')
							tx = transceivers[self.transceiver_id]['status']['frames']['tx']
							rx = transceivers[self.transceiver_id]['status']['frames']['rx']
							ok = transceivers[self.transceiver_id]['status']['responses']['0']
							error = transceivers[self.transceiver_id]['status']['responses']['1']
							ic = transceivers[self.transceiver_id]['status']['responses']['2']
							ip = transceivers[self.transceiver_id]['status']['responses']['3']
							tx_failure = transceivers[self.transceiver_id]['status']['responses']['4']
						publish = rr_database[self.transceiver_id][frame_id]['publish']
						base_payload = {'deviceid':deviceid, 'location':location, 'sublocation':sublocation}	
						series = transceivers[self.transceiver_id]['settings']['series']
						# Process local and remote AT diagnostics responses present in message
						for key, value in self.message.iteritems():
							if key == 'command':
								# Below iteration are for requests that origianted from us
								for category, commands in self.commands[series].iteritems():
									for command, desc in commands.iteritems():
										if command == value:
											payload = {}
											payload.update(base_payload)
											mc = command.lower()
											if command == 'DB':
												mc = 'rssi'
												val = utils.get_rssi(self.message['parameter'])
												if self.message['id'] == 'at_response':
													payload.update({'rx':rx, 'tx':tx, 'ok':ok, 'error':error, 'inv_comm':ic, 'inv_para':ip, 'tx_failure':tx_failure})
												# Publish the device's MAC address
												if self.message['id'] == 'remote_at_response':
													payload.update({'mac':mac})
											elif command in ['EA', 'EC','ID','IR','D0','D1','D2','D3','D4','D5','D6','D7','D8','D9','D10','DP','SO','IT','IC','P0','P1','PT','RP']:
												val = int(utils.bytetohex(self.message['parameter']), 16)
											elif command in ['SP','ST','SN','SM','SO']:
												val = utils.bytetohex(self.message['parameter'])
											elif command == '%V':
												mc = 'battery_voltage'
												val = int(utils.bytetohex(self.message['parameter']), 16)
											elif command == 'TP':
												mc = 'module_temperature'
												val = int(utils.bytetohex(self.message['parameter']), 16)
											elif command in ['NI']:
												val = self.message['parameter']
											else:
												#val = utils.bytetohex(self.message['parameter'])
												val = ''.join("{:02X}".format(ord(c)) for c in self.message['parameter']).lower()
											descri = self.commands[series][category][command]
											ed_status[deviceid][category].update({descri:val})
											payload.update({mc:val})
									# Send diagnostics data to MQTT thread for publishing
									if publish and 'mc' in locals() and 'val' in locals() and 'payload' in locals():
										publish(payload)
										del payload
			elif status == '4':
				q5 = queue.copy()
				for request_id, message in q5.iteritems():
					if frame_id in message.itervalues() and self.transceiver_id in message.itervalues():
						timestamp = self.now.strftime('%Y-%m-%d %H:%M:%S.%f')
						retry = queue[request_id]['retry'] + 1
						priority = queue[request_id]['priority']
						message = {'last_retry':timestamp, 'retry':retry, 'status':4, 'timestamp':timestamp}
						queue[request_id].update(message)
						request = {request_id:queue[request_id]}
						priority_queue.put(request, priority) 
								
			# Stop retrans of responses with status  1 = ERR , 2 = Invalid command, 3 = Invalid parameter 
			elif status in ['1', '2', '3']:
				tempq = queue.copy()
				for request_id, message in tempq.iteritems():
					if frame_id in message.itervalues() and self.transceiver_id in message.itervalues():
						queue[request_id]['status'] = 0

			# Update stats  
			transceivers[self.transceiver_id]['status']['responses'][status] += 1
		except Exception:
			sys.excepthook(*sys.exc_info())

### This thread retireves and publishes external weather reference at time intervals
class publish_openweather(threading.Thread):
	def __init__(self, name, interval):
		threading.Thread.__init__(self, name = name)
		self.interval = interval
		return
	def run(self):
		import yaml
		logger.info("Publishing OpenWeather wether reports at '%s' secs intervals..." % self.interval)
		while True:
			try:
				## update weather first
				payload ={}
				openweather = utils.get_weather(config['openweathermap_city'], config['openweathermap_appid'])
				payload_base = {'deviceid':'openweather','location':'puerto_varas','sublocation':'outside'}
				payload.update(openweather)
				payload.update(payload_base)
				publish(payload)
				time.sleep(self.interval)
			except Exception:
				sys.excepthook(*sys.exc_info())

if __name__ == "__main__":
	print "XQTT is spinning. Check /var/log/xqtt.log for more details..."
	# Load environment variables and config parameters from file config.cfg
	environment_load()
	# Load and create radio_db dictionaries
	radio_db_load()
	if runmode == 'kapacitor':
		if config['kapacitor_enabled']:
			# Generate Kapacitor alerts generator
			logger.warning("Adding Kapacitor alerts...")
			print("Adding Kapacitor alerts...")
			utils.kapacitor_load('add', radio_db_dict, config)
		elif config['kapacitor_enabled'] == False:
			# Generate Kapacitor alerts generator
			logger.warning("Removing Kapacitor alerts...")
			print("Removing Kapacitor alerts...")
			utils.kapacitor_load('delete', radio_db_dict, config)
		quit()
	# Perfrom network checks and wait there until we can reach the MQTT broker
	check_network()
	# Create xbee transceiver objects
	transceiver_load()
	# Connect to influxdb if enabled
	#reset_xbee('zr0')
	if config['influxdb_enabled']:
		influxdb_connect()
	if config['elasticsearch_enabled']:
		elasticsearch_connect()
	try:
		# Start MQTT thread here
		thread = mqtt_connect(name = 'mqtt_thread')
		thread.daemon = True
		thread.start()
		logger.warning("Started mqtt_connect thread...")
		# Web server thread here
		thread = http_server(config['http_ip'], config['http_port'])
		thread.start()
		logger.warning("Started http_server thread...")
		# Start read_xbee_frame threads if not already running, one per xbee modem configured
		for transceiver, attributes in transceiver_rfs.iteritems():
			# Dispatcher thread
			thread_name = str(transceiver)+"_dispatcher_thread"
			thread = dispatcher(config['mqtt_publish_interval'], thread_name)
			thread.daemon = True
			thread.start()
			logger.warning("Started dispatcher thread for xbee transceiver '%s' ..." % transceiver)
			time.sleep(0.5)
			thread_name = str(transceiver)+"_coordinator_thread"
			thread = read_xbee_frame(attributes['xbee_object'],  attributes['series'], attributes['transceiver_id'], thread_name)
			thread.daemon = True
			thread.start()
			logger.warning("Started a read_xbee_frame thread for xbee transceiver '%s' ..." % transceiver)
			time.sleep(0.5)
			# Optional tranceiver specific threads here
			if config['interrogator_enabled']:
				thread_name = str(transceiver)+"_interrogator_thread"
				thread = interrogator(attributes['xbee_object'], attributes['transceiver_id'], config['interrogator_interval'], thread_name)
				thread.daemon = True
				thread.start()
				logger.warning("Started interrogator thread for xbee transceiver '%s' ..." % transceiver)
				time.sleep(0.5)
		# Start read_gateway_frame threads if not already running, one per mqtt gateway configured
		for gateway, attributes in mqtt_gateways.iteritems():
			thread_name = str(gateway)+"_gateway_thread"
			thread = read_gateway_frame(gateway, thread_name)
			thread.daemon = True
			thread.start()
			logger.warning("Started a read_gateway_frame thread for gateway '%s' ..." % gateway)
			time.sleep(0.5)
		# Start global optional threads
		if config['openweather_enabled']:
			# OpenWeather thread
			thread = publish_openweather('publish_openweather', config['openweather_interval'])
			thread.daemon = True
			thread.start()
			logger.warning("Started publish_openweather thread...")
		# Supervisor thread starts last
		thread = supervisor(name = 'supervisor')
		thread.daemon = True
		thread.start()
		logger.warning("Started supervisor thread...")
		'''
		while True:
			print "Queue:"
			print json.dumps(queue, sort_keys=True, indent=4)
			print "RR Database:"
			print json.dumps(rr_database, sort_keys=True, indent=4)
			print "Number of active threads: %s" % threading.active_count()
			print get_threads()
			time.sleep(30.0 - ((time.time() - starttime) % 30.0))'''

	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print "Got exception on main handler:"
		mqttc.loop_stop()
		mqttc.disconnect()
		for transceiver, settings in transceiver_rfs.iteritems():
			attributes['serial'].close()
		raise

	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print "Other error or exception occurred!"
		mqttc.loop_stop()
		mqttc.disconnect()
		for transceiver, settings in transceiver_rfs.iteritems():
			attributes['serial'].close()

	while True:
		time.sleep(1)
		pass
