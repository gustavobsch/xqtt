#!/usr/bin/python
import logging, random, sys, json, httplib, requests, time, sensors, yaml
from Queue import PriorityQueue
logger = logging.getLogger('xqtt')
logger.info("Library utils.py started...")

class queue(PriorityQueue):
	def __init__(self):
		PriorityQueue.__init__(self)
		self.counter = 0

	def put(self, item, priority):
		PriorityQueue.put(self, (priority, self.counter, item))
		self.counter += 1

	def get(self, *args, **kwargs):
		_, _, item = PriorityQueue.get(self, *args, **kwargs)
		return item

def decode_payload(frame, radio_db):
	try:
		### Initiate data containers
		frame_payload = {}
		payload = {}
		# Get deviceid
		deviceid = radio_db.get(frame['source_addr_long'], 'id')
		# Process joining notification frames
		if frame['id'] == 'node_id_indicator':
			for key, value in frame.iteritems():
				if key in ['node_id', 'source_addr_long']:
					val = value
				else:
					val = ''.join("{:02X}".format(ord(c)) for c in value).lower()
				frame_payload.update({key:val})
			frame_payload.update({'id':'join_notification'})
		# Process json payload of end devices working in AT or API mode
		elif frame['id'] in ['rx', 'rx_long_addr']:
			try:
				frame_payload = yaml.load(frame['rf_data'])
				frame_payload = decode(frame_payload)
			except yaml.YAMLError as exc:
				logger.exception("Failed parsing frame '%s' check JSON format!" % (frame, exc))
		# Process frame with sensor payload
		elif frame['id'] == 'rx_io_data_long_addr':
			### If no explicit vref then default to 1.2 volts
			if not radio_db.has_option(frame['source_addr_long'], 'vref'):
				vref = 1.2
			else:
				vref =  radio_db.getfloat(frame['source_addr_long'], 'vref')
			### ADC sensor data processing starts here
			for dio, value in frame['samples'][0].iteritems():
				# Get dio number and prepend D to it
				dio = "D" + str(int(filter(str.isdigit, dio)))
				# Check if this is a dio only frame
				dio_only = is_dio_only(frame['samples'][0])
				if not radio_db.has_option(frame['source_addr_long'], dio) and not value:
					# Ignore this readout since sensor is not configured and value is False
					continue
				elif not radio_db.has_option(frame['source_addr_long'], dio):
					logger.exception("Missing sensor definition for line '%s', device '%s' in radio_db config file!." % (dio, deviceid))
					continue
				sensor = radio_db.get(frame['source_addr_long'], dio)
				#if not dio_only and value:
				if not dio_only:
					if sensor[:5] == "tmp36":
						readout = round(sensors.get_tmp36(value, vref), 2)
						if readout > 80 or readout < -10:
							logger.exception("Skipping bogus sensor readout from device '%s'!, offending readout: ADC='%s', calculated temperature='%s', line'%s'." % (deviceid, value, readout, dio))
							continue
						frame_payload.update({'temperature':readout})
					elif sensor[:8] == "temt6000":
						readout = round(sensors.get_temt6000(value, vref), 2)
						frame_payload.update({"lux":readout})
					elif sensor[:6] == "lm35dz":
						readout = sensors.get_lm35dz(value, vref)
						if readout > 80 or readout < -10:
							logger.exception("Skipping bogus sensor readout from device '%s'!, offending readout: ADC='%s', calculated temperature='%s', line'%s'." % (deviceid, value, readout, dio))
							continue
						frame_payload.update({'temperature':readout})
					elif sensor[:10] == "lm393_rain":
						readout = round(sensors.get_lm393_rain(value, vref), 2)
						frame_payload.update({sensor:readout})
					elif sensor[:5] == "plant":
						readout = round(sensors.get_hygrometer(value, vref), 2)
						frame_payload.update({sensor:readout})
					elif sensor == "battery_voltage_internal":
						readout = round(sensors.get_volt(value, vref) * 1000, 2)
						frame_payload.update({sensor:readout})
					elif sensor == "battery_voltage":
						full_battery_voltage =  radio_db.getfloat(frame['source_addr_long'], 'battery_voltage')
						readout = round(valmap(value, 0.0, 1023.0, 0.0, full_battery_voltage) * 1000, 2)
						frame_payload.update({sensor:readout})
					elif sensor == "sound_db":
						#volt = get_volt(int(item[1]))
						#readout = round(16.801 * math.log(volt) + 9.872, 4)
						readout = round(sensors.get_volt(value, vref) / 10, 2)
						frame_payload.update({sensor:readout})
					elif sensor == "pir" and value == True: # True means pir detected, False means no pir
						frame_payload.update({'presence':1, 'pir':1})
					#elif sensor == "pir" and value == False: # True means pir detected, False means no pir
					#	frame_payload.update({'pir':0})
		return frame_payload
	except Exception:
		sys.excepthook(*sys.exc_info())
	except:
		logger.exception("Other error or exception occurred!")

def decode_gateway_payload(payload, config):
	try:
		drop_element_list = ['unixtime', 'latlng']
		new_payload = {}
		for key, value in payload.iteritems():
			if key == 'latlng':
				if value[0] * value[1] == 0:
					new_payload.update({'latitude':float(config['latitude']), 'longitude':float(config['longitude'])})
				else:
					new_payload.update({'latitude':value[0], 'longitude':value[1]})
		# Update payload with new payload
		payload.update(new_payload)
		# Finally delete unwated items
		for item in drop_element_list:
			if item in payload:
				del payload[item]
		return payload
	except Exception:
		sys.excepthook(*sys.exc_info())
	except:
		logger.exception("Other error or exception occurred!")
		
def decode(payload):
	for key, value in payload.iteritems():
		payload[sensor_map(key)] = payload.pop(key)
		payload[sensor_map(key)] = convert_units(sensor_map(key), payload[sensor_map(key)])
	if 'pir' in payload:
		payload.update({'presence':1})
	return payload

def convert_units(sensor, value):
	if (sensor in ['temperature', 'dust','altitude', 'humidity', 'external_water_tank', 'r620_outlet', 'exposed_temp_sensor', 'bed_temperature']) and (isinstance(value, int)):
		return value / 100.0
	elif (sensor in ['rssi']) and value > 0:
		return value * -1
	elif (sensor in ['rain_volt']) and value > 100:
		return value / 100.0
	else:
		return value

def sensor_map(sensor):
	sensor_map = {
		# vcc measurements 1 - 9
		'vcc': 						'1',
		'battery_per': 				'2',
		'battery_voltage':			'3',
		'battery_level':			'4',
		# weather sensors 10 - 19
		'temperature':				'10',
		'onboard_temp':				'11',
		'pressure':					'12',
		# humidty, rain 20-29
		'humidity':					'20',
		'rain_volt':				'21',
		# soil sensors 30 - 39
		# air sensors 40 - 49
		'dust':						'40',
		'co2':						'41',
		'gas':						'42',
		'smoke':					'43',
		# gps related 50 - 69
		'altitude':					'50',
		# ascelerometers 70 - 79
		# color - light sensors 80 -89
		'r':						'80',
		'g':						'81',
		'b':						'82',
		'c':						'83',
		'lux':						'84',
		'color_temp':				'85',
		# signal strength 90 - 99
		'rssi':						'90',
		'ec':						'91',
		'ea':						'92',
		# other stuff 100 - 130
		'external_water_tank':		'100',
		'exposed_temp_sensor':		'101',
		'bed_temperature':			'102',
		'outside_temperature':		'103',
		'r620_outlet':				'104',
		# presence 
		'pir':						'131',
	}
	for key, value in sensor_map.items():
		if sensor == value:
			return key
	return sensor

def kapacitor_task_body(mode, kapacitor):
	script = kapacitor['script']
	script += kapacitor['name']
	script += kapacitor['details']
	script += kapacitor['message']
	script += kapacitor['period']
	script += kapacitor['trigger']
	script += kapacitor['var']
	script += """
	var rp = 'autogen'
	var groupBy = []
	var idVar = name
	var idTag = 'alertID'
	var levelTag = 'level'
	var messageField = 'message'
	var durationField = 'duration'
	var outputDB = 'chronograf'
	var outputRP = 'autogen'
	var outputMeasurement = 'alerts'
	var data = stream
		|from()
			.database(db)
			.retentionPolicy(rp)
			.measurement(measurement)
			.groupBy(groupBy)
			.where(whereFilter)"""
	if mode == "battery":
		script += "|eval(lambda: \"battery_voltage\").as('value')"
	script += "var trigger = data"
	if mode == "battery":
		script += " |alert()\n.crit(lambda: \"value\" < crit)"
	elif mode == "deadman":
		script += "|deadman(threshold, period)"
	script += """
			.message(message)
			.id(idVar)
			.idTag(idTag)
			.levelTag(levelTag)
			.messageField(messageField)
			.stateChangesOnly()
			.durationField(durationField)
			.details(details)
			.email()"""
	script += ".to('"+kapacitor['email']+"')"
	script += """
			.slack()
			.channel('#alerts')
	trigger
		|eval(lambda: \"emitted\")
			.as('value')
			.keep('value', messageField, durationField)
		|eval(lambda: float(\"value\"))
			.as('value')
			.keep()
		|influxDBOut()
			.create()
			.database(outputDB)
			.retentionPolicy(outputRP)
			.measurement(outputMeasurement)
			.tag('alertName', name)
			.tag('triggerType', triggerType)
	trigger
		|httpOut('output')"""
	return script

def kapacitor_load(mode, radio_db_dict, config):
	pusher = kapacitor_api(config)
	kapacitor_tasks = {}
	# Load tasks from radiodb dict
	for device, attributes in radio_db_dict.iteritems():
		for parameter, value in attributes.iteritems():
			if parameter == 'alerting' and value == 'True':
				kapacitor_tasks.update({attributes['id']+"_"+device:{'deviceid':attributes['id'], 'sleep_interval':int(attributes['sleep_interval']), 'battery_voltage':float(attributes['battery_voltage'])}})
	if mode == 'add':
		kapacitor = {}
		# Generate deadman tasks
		for taskid, details in kapacitor_tasks.iteritems():
			# Fill variables for dead man task scripts first
			kapacitor['script'] = "var db = '"+config['kapacitor_database']+"'\nvar whereFilter = lambda: (\"deviceid\" == '"+details['deviceid']+"')\nvar measurement = '"+config['kapacitor_measurement']+"'"
			kapacitor['name'] =  "var name = 'XQTT generated Deadman alert for "+taskid+"'"
			kapacitor['details'] = "var details = 'IoT device "+details['deviceid']+" is gone'"
			kapacitor['message'] = "var message = '{{.Level}}: Device gone {{.TaskName}}'"
			kapacitor['period'] = "var period = 30m"
			kapacitor['trigger'] = "var triggerType = 'deadman'"
			kapacitor['var'] = "var threshold = 0.0"
			kapacitor['email'] = config['kapacitor_notifications']
			taskid +="_deadman"
			# Generate json payload task
			json_task = {"id" : taskid,
						"type" : "stream",
						"dbrps": [{"db": config['kapacitor_database'],
								"rp": "autogen"
								 }],
						"script": kapacitor_task_body("deadman", kapacitor), 
						"vars": {},
						"status" : "enabled",
						"executing" : True}
			# Remove any copy of the task first
			pusher.remove(taskid)
			# Push the new task
			pusher.set(json_task)
			time.sleep(0.3)
		# Generate low battery alert tasks
		for taskid, details in kapacitor_tasks.iteritems():
			if details['battery_voltage'] >= 4:
				bt = "3600"
			elif details['battery_voltage'] < 4:
				bt = "1800"
			# Fill variables for dead man task scripts first
			kapacitor['script'] = "var db = '"+config['kapacitor_database']+"'\nvar whereFilter = lambda: (\"deviceid\" == '"+details['deviceid']+"')\nvar measurement = '"+config['kapacitor_measurement']+"'"
			kapacitor['name'] =  "var name = 'XQTT generated Low Battery alert for "+taskid+"'"
			kapacitor['details'] = "var details = 'IoT device "+details['deviceid']+" battery running low'"
			kapacitor['message'] = "var message = '{{.Level}}: Battery low {{.TaskName}}'"
			kapacitor['period'] = "var period = 10m"
			kapacitor['trigger'] = "var triggerType = 'threshold'"
			kapacitor['var'] = "var crit = "+bt
			kapacitor['email'] = config['kapacitor_notifications']
			taskid +="_battery"
			# Generate json payload task
			json_task = {"id" : taskid,
						"type" : "stream",
						"dbrps": [{"db": config['kapacitor_database'],
								"rp": "autogen"
								 }],
						"script": kapacitor_task_body("battery", kapacitor), 
						"vars": {},
						"status" : "enabled",
						"executing" : True}
			# Remove any copy of the task first
			pusher.remove(taskid)
			# Push the new task
			pusher.set(json_task)
			time.sleep(0.3)
	elif mode == 'delete':
		# Generate task scripts
		for taskid, details in kapacitor_tasks.iteritems():
			# Remove copies of the task 
			pusher.remove(taskid+"_battery") 
			pusher.remove(taskid+"_deadman") 
			time.sleep(0.3)

# Kapacacitor task class
class kapacitor_api(object):
	def __init__(self, config):
		self.config = config
		self.server = self.config['kapacitor_hostname']
		self.port = self.config['kapacitor_port']
	def get(self, data):
		path = '/kapacitor/v1/tasks'
		ret = self.rest_call({}, path, 'GET')
		return json.loads(ret[2])

	def set(self, data):
		path = '/kapacitor/v1/tasks'
		ret = self.rest_call(data, path, 'POST')
		return ret[0] == 200

	def remove(self, data):
		path = "/kapacitor/v1/tasks/"+data
		ret = self.rest_call(data, path, 'DELETE')
		return ret[0] == 200
  
	def rest_call(self, data, path, action):
		headers = {
			'Content-type': 'application/json',
			'Accept': 'application/json',
			}
		body = json.dumps(data)
		conn = httplib.HTTPConnection(self.server, self.port)
		conn.request(action, path, body, headers)
		response = conn.getresponse()
		ret = (response.status, response.reason, response.read())
		print ret
		conn.close()
		return ret


def get_weather(city, appid):
	import json, requests
	try:
		url = "http://api.openweathermap.org/data/2.5/weather?id=%s&APPID=%s&units=metric" % (city, appid)
		request = json.loads(requests.get(url).text)
		weather = {}
		for key, value in request['wind'].iteritems():
			if key == 'speed':
				weather.update({'wind_speed':value})
			elif key == 'deg':
				weather.update({'wind_direction':value})
		for key, value in request['main'].iteritems():
			if key == 'temp':
				weather.update({'temperature':value})
			elif key == 'humidity':
				weather.update({'humidity':value})
			elif key == 'pressure':
				weather.update({'pressure':value})
		return weather
	except Exception:
		sys.excepthook(*sys.exc_info())

def chop_comment(line):
	c_backslash = '\\'
	c_dquote = '"'
	c_comment = '#'
	space = ' '
	# a little state machine with two state varaibles:
	in_quote = False  # whether we are in a quoted string right now
	backslash_escape = False  # true if we just saw a backslash
	for i, ch in enumerate(line):
		if not in_quote and ch == c_comment:
			# not in a quote, saw a '#', it's a comment.  Chop it and return!
			return line[:i]
		elif not in_quote and ch == space:
			# not in a quote, saw a '#', it's a comment.  Chop it and return!
			return line[:i]
		elif backslash_escape:
			# we must have just seen a backslash; reset that flag and continue
			backslash_escape = False
		elif in_quote and ch == c_backslash:
			# we are in a quote and we see a backslash; escape next char
			backslash_escape = True
		elif ch == c_dquote:
			in_quote = not in_quote
	return line
    
def onoff(gpio, status):
	if gpio == 'buz':
		if status == 'off':
			parameter = '05' # convert on status to xbee at mode 5 which is high 
		elif status == 'on':
			parameter = '04' # convert off to xbee at mode 4 which is low
	elif gpio == 'relay':
		if status == 'on':
			parameter = '04'
		elif status == 'off':
			parameter = '05'
	elif gpio == 'led':
		if status == 'off':
			parameter = '04'
		elif status == 'on':
			parameter = '05'
	return parameter

def is_dio_only(samples):
	for dio, value in samples.iteritems():
		if isinstance(value, bool):
			continue
		elif isinstance(value, int):
			return False
	return True

def randomid(r):
	if r == 1:
		ran_id = bytearray(random.getrandbits(8) for i in range(r))
	else:
		ran_id = "".join(['0123456789abcdef'[random.randint(0,0xF)] for _ in range(r) ])
	return ran_id
        
def get_sublocation(sensor, sublocation):
	if '-' in sensor:
		sublocation = sensor.split("-",1)[1]
		return sublocation
	else:
		return sublocation

def get_rssi(data):
	rssi = int(bytetohex(data), 16) * -1
	return rssi

def bytetoint(byte):
	if byte is None:
		return None
	#Determines whether to use ord() or not to get a byte's value.
	if hasattr(byte, 'bit_length'):
		# This is already an int
		return byte
	return ord(byte) if hasattr(byte, 'encode') else byte[0]

def intToByte(i):
	# Determines whether to use chr() or bytes() to return a bytes object.
	return chr(i) if hasattr(bytes(), 'encode') else bytes([i])

def bytetohex(s):
	a = list(s)
	s = str()
	for i in range(0,len(a)):
		s = s+hex(bytetoint(a[i]))[2:]
	return s

def stringToBytes(s):
	#	Converts a string into an appropriate bytes object
	return s.encode('ascii')

def valmap(value, istart, istop, ostart, ostop):
	volt = round(ostart + (ostop - ostart) * ((value - istart) / (istop - istart)), 2)
	return volt

if __name__ == "__main__":
	print("This module is not executable, please run xqtt.py instead")
