#!/usr/bin/python
import logging
import sys

def setup_logger(name):
	formatter = logging.Formatter('%(asctime)s-%(levelname)s-%(module)s-%(threadName)s-%(message)s')
#	handler = logging.FileHandler('/var/log/xqtt.log')
	handler = logging.StreamHandler(sys.stdout)
	handler.setFormatter(formatter)
	logger = logging.getLogger(name)
	logger.setLevel(logging.WARNING)
	#logger.setLevel(logging.DEBUG)
	#logger.setLevel(logging.INFO)
	logger.addHandler(handler)
	logger.info("Library logs.py started...")
	return logger

if __name__ == "__main__":
	print("This module is not executable, please run xqtt.py instead")

