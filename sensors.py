#!/usr/bin/python
import logging
logger = logging.getLogger('xqtt')
logger.info("Library sensors.py started...")

def get_volt(data, vref):
	volt = round((data / 1023.0) * vref, 2)
	return volt

def get_tmp36(data, vref):
	# Temperature  calculated as: adc multiplied by vcc and then substract 500 and divided by ten 
	temp = (data * vref - 500) / 10.0
	return round(temp, 2)

def get_lm35dz(data, vref):
	# Temperature  calculated as 10mV / C
	temp = ((data / 1023.0) * vref) * 100
	return round(temp, 2)

def get_temt6000(data, vref):
	if vref == 1.2:
		vref = 3.3
	volt = get_volt(data, vref)
	amps = volt / 10000.0 
	microamps = amps * 1000000
	lux = round(microamps * 6.0, 2)
	return lux

def get_lm393_rain(data, vref):
	if vref == 1.2:
		vref = 3.3
	volt = get_volt(data, vref)
	return volt

def get_mq(data, vref):
	volt = get_volt(data, vref)
	value = round(((volt * 10) * 2.34), 2)
	return value
	
def get_hygrometer(data, vref):
	hum_per = round(((get_volt(data, vref) - 0) / (2.7 - 0) ) * (100 - 0) + 0, 2)
	if hum_per < 200:
		return hum_per
	else:
		return 0

def get_hr202(data, vref):
	#volt = get_volt(data, vref)
	humidity = get_volt(data, vref)
	#humidity = -47.65 * (volt) + 290.63
	return humidity

if __name__ == "__main__":
	print("This module is not executable, please run xqtt.py instead")
